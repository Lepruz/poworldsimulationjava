package com.company;

public abstract class Animal extends Organism{
    public Animal(int x, int y, World world) {
        super(x, y, world);
    }
    public void akcja() {
        int temp_x = x + random.nextInt(3) - 1;
        int temp_y = y + random.nextInt(3) - 1;
        if (temp_x == x && temp_y == y)
            return;
        if (canMove(temp_x, temp_y) == EMPTY) {
            world.getEvents().add(this.toString() + " moved to (" + temp_x +", " + temp_y+")");
            x = temp_x;
            y = temp_y;
        } else if (canMove(temp_x, temp_y) == COLLISION) {
            world.findOrg(temp_x, temp_y).kolizja(this);
        }
    }
}
