package com.company.Animals;


import com.company.Animal;
import com.company.World;

import java.awt.*;

public class Wolf extends Animal {
    public Wolf(int x, int y, World world) {
        super(x, y, world);
        this.strength=9;
        this.initiative=5;
        this.type="Wolf";
        this.color=Color.BLACK;
    }

    public Wolf(int x, int y, World world,int strength) {
        super(x, y, world);
        this.strength=strength;
        this.initiative=5;
        this.type="Wolf";
        this.color = Color.BLACK;
    }


    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Wolf(xx,yy,world));
    }
}
