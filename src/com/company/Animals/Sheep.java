package com.company.Animals;

import com.company.Animal;
import com.company.World;

import java.awt.*;

public class Sheep extends Animal {
    public Sheep(int x, int y, World world) {
        super(x, y, world);
        this.strength=4;
        this.initiative=4;
        this.type="Sheep";
        this.color=Color.GRAY;
    }

    public Sheep(int x, int y, World world,int strength) {
        super(x, y, world);
        this.strength=strength;
        this.initiative=4;
        this.type="Sheep";
        this.color=Color.GRAY;
    }


    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Sheep(xx,yy,world));
    }

}
