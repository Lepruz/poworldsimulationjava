package com.company.Animals;

import com.company.Animal;
import com.company.Organism;
import com.company.World;

import java.awt.*;

public class Antelope extends Animal {
    public Antelope(int x, int y, World world, int strength) {
        super(x, y, world);
        this.initiative=4;
        this.strength=strength;
        this.type="Antelope";
        this.color=new Color(110,77,12);
    }
    public Antelope(int x, int y, World world) {
        super(x, y, world);
        this.initiative=4;
        this.strength=4;
        this.type="Antelope";
        this.color=new Color(110,77,12);
    }

    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Antelope(xx,yy,world));
    }

    @Override
    public void akcja() {
        int multiplier = random.nextInt(2) + 1;
        int temp_x = x + (random.nextInt(3) - 1)*multiplier;
        int temp_y = y + (random.nextInt(3) - 1)*multiplier;
        if (temp_x == x && temp_y == y)
            return;
        if (canMove(temp_x, temp_y) == EMPTY) {
            world.getEvents().add(this.toString() + " moved to (" + temp_x +", " + temp_y+")");
            x = temp_x;
            y = temp_y;
        }
        else if (canMove(temp_x, temp_y) == COLLISION) {
            world.findOrg(temp_x, temp_y).kolizja(this);
        }
    }

    @Override
    public void kolizja(Organism attacker) {
        int chance = random.nextInt(2);

        if (attacker.getType() == type) {
            this.multiplication();
            return;
        }

        if (chance==1) {
            for (int i = -1; i < 2; i++) {
                for (int l = -1; l < 2; l++) {
                    if (canMove(x + i, y + l) == EMPTY) {
                        world.getEvents().add(attacker.toString() + " moved to (" + x +", " + y+")");
                        attacker.setX(x);
                        attacker.setY(y);
                        int temp_x=x+i;
                        int temp_y=y+l;
                        world.getEvents().add(this.toString() + " run to (" + temp_x + ", " + temp_y +")");
                        x = temp_x;
                        y = temp_y;
                        return;
                    }
                }
            }
        }
        super.kolizja(attacker);
    }

}


