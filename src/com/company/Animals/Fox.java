package com.company.Animals;

import com.company.Animal;
import com.company.World;

import java.awt.*;


public class Fox extends Animal {
    public Fox(int x, int y, World world) {
        super(x, y, world);
        this.initiative=7;
        this.strength=3;
        this.type="Fox";
        this.color=Color.ORANGE;
    }
    public Fox(int x, int y, World world, int strength) {
        super(x, y, world);
        this.initiative=7;
        this.strength=strength;
        this.type="Fox";
        this.color=Color.ORANGE;
    }

    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Fox(xx,yy,world));
    }

    @Override
    public void akcja() {
        int temp_x = x + random.nextInt(3) - 1;
        int temp_y = y + random.nextInt(3) - 1;
        if (temp_x == x && temp_y == y)
            return;
        if (canMove(temp_x, temp_y) == EMPTY) {
            world.getEvents().add(this.toString() + " moved to (" + temp_x +", " + temp_y+")");
            x = temp_x;
            y = temp_y;
        }
        else if (canMove(temp_x, temp_y) == COLLISION && world.findOrg(temp_x, temp_y).getStrength()<=strength) {
            world.findOrg(temp_x, temp_y).kolizja(this);
        }
    }
}
