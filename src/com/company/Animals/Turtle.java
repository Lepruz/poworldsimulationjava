package com.company.Animals;

import com.company.Animal;
import com.company.Organism;
import com.company.World;

import java.awt.*;

public class Turtle extends Animal {
    public Turtle(int x, int y, World world) {
        super(x, y, world);
        this.strength=2;
        this.initiative=2;
        this.type="Turtle";
        this.color=new Color(7,107,18);
    }

    public Turtle(int x, int y, World world,int strength) {
        super(x, y, world);
        this.strength=strength;
        this.initiative=2;
        this.type="Turtle";
        this.color=new Color(7,107,18);
    }

    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Turtle(xx,yy,world));
    }

    @Override
    public void akcja() {
        int chance = random.nextInt(4);
        if (chance == 3) {
            super.akcja();
        }
    }

    @Override
    public void kolizja(Organism attacker) {
        if (attacker instanceof Turtle){
            multiplication();
            return;
        }
        if (attacker.getStrength() >= 5 && attacker.getStrength() >= this.getStrength()) {
            this.setDead(true);
            if (this.getInitiative() != 0) {}
//            save << attacker->toString() << " defeated " << this->toString() << endl;
		    else
//            save << attacker->toString() << " ate " << this->toString() << endl;
            attacker.setX(x);
            attacker.setY(y);
        }
	else {
//            save << this->toString() << " blocked " << attacker->toString() << endl;
        }
    }
}
