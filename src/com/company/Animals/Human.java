package com.company.Animals;

import com.company.Animal;
import com.company.World;

import java.awt.*;
import java.util.Scanner;

public class Human extends Animal {
    private int direction;
    private static final int UP = 1;
    private static final int DOWN = 2;
    private static final int LEFT = 3;
    private static final int RIGHT = 4;

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public Human(int x, int y, World world) {
        super(x, y, world);
        this.initiative=4;
        this.strength=4;
        this.type="Human";
        this.color=new Color(45,187,91);
    }
    public Human(int x, int y, World world, int strength) {
        super(x, y, world);
        this.initiative=4;
        this.strength=strength;
        this.type="Human";
        this.color=new Color(45,187,91);
    }

    public static int getUP() {
        return UP;
    }

    public static int getDOWN() {
        return DOWN;
    }

    public static int getLEFT() {
        return LEFT;
    }

    public static int getRIGHT() {
        return RIGHT;
    }

    @Override
    public void akcja() {
        int temp_x = x;
        int temp_y = y;
        if (direction == UP) {
            temp_y--;
        }
        else if (direction == DOWN) {
            temp_y++;
        }
        else if (direction == RIGHT) {
            temp_x++;
        }
        else if (direction == LEFT) {
            temp_x--;
        }
        if (temp_x == x && temp_y == y)
            return;
        if (canMove(temp_x, temp_y) == EMPTY) {
            world.getEvents().add(this.toString() + " moved to (" + temp_x +", " + temp_y+")");
            x = temp_x;
            y = temp_y;
        } else if (canMove(temp_x, temp_y) == COLLISION) {
            world.findOrg(temp_x, temp_y).kolizja(this);
        }
    }

    @Override
    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Human(xx,yy,world));
    }
}
