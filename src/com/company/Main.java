package com.company;

import com.company.Animals.Antelope;
import com.company.Animals.Fox;
import com.company.Animals.Human;
import com.company.Animals.Wolf;
import com.company.Graphics.MyDraw;
import com.company.Plants.SosnowskiHogweed;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int w,h;
        List<Organism> tab = new ArrayList<>();
        List<String> events=new ArrayList<>();
        Scanner input = new Scanner(System.in);
        w=input.nextInt();
        h=input.nextInt();
        World world = new World(w,h,tab, events);

        world.getTab().add(new Human(9,5,world));
        MyDraw display = new MyDraw(w*50,h*50,world);

    }
}
