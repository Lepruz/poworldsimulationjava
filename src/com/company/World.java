package com.company;

import com.company.Animals.*;
import com.company.Graphics.MyDraw;
import com.company.Plants.*;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class World {
    private int w,h, superpower;
    private List<Organism> tab;
    private List<String> events;

    public World(int w, int h, List<Organism> tab, List<String> events) {
        this.w = w;
        this.h = h;
        this.tab = tab;
        this.events=events;
    }


    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getSuperpower() {
        return superpower;
    }

    public void setSuperpower(int superpower) {
        this.superpower = superpower;
    }

    public List<Organism> getTab() {
        return tab;
    }

    public void setTab(List<Organism> tab) {
        this.tab = tab;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }

    public boolean findOrgBool(int x, int y) {
        for (int i = 0; i < tab.size(); i++) {
            if (tab.get(i).getX() == x && tab.get(i).getY() == y && tab.get(i).isDead()==false)
            return true;
        }
        return false;
    }

    public Organism findOrg(int x, int y) {
        for (int i = 0; i < tab.size(); i++) {
            if (tab.get(i).getX() == x && tab.get(i).getY() == y && !tab.get(i).isDead())
            return tab.get(i);
        }
        return null;
    }

    public Human findHuman() {
        for (int i = 0; i < tab.size(); i++) {
            if (tab.get(i) instanceof Human && !tab.get(i).isDead())
                return (Human)tab.get(i);
        }
        return null;
    }


    public void turn() {

        //supermoc
        if (superpower == 0 && findHuman()!=null) {
            findHuman().setStrength(findHuman().getStrength() + 5);
            superpower++;
        }
        else if (superpower > 0 && superpower <=5 && findHuman() != null) {
            findHuman().setStrength(findHuman().getStrength() - 1);
            superpower++;
        }
        else if (superpower >5 && superpower<10) {
            superpower++;
        }
        else if (superpower == 10) {
            superpower = -1;
        }
        if (findHuman()==null) {
            superpower = -1;
        }


        events = new ArrayList<>();

        for (int j = 7; j >= 0; j--) {
            for (int i = 0; i < tab.size(); i++) {
                if (tab.get(i).isDead == false && tab.get(i).getInitiative()==j) {
                    tab.get(i).akcja();
                }
            }
        }

        for (int i = 0; i < tab.size();) {
            if (tab.get(i).isDead()) {
                tab.get(i).removeItself();
                i = 0;
            }
		else
            i++;
        }
    }

    public void saveGame() throws FileNotFoundException {
        PrintWriter save = new PrintWriter("save.txt");
        save.println(superpower);

        for(int i=0; i<tab.size();i++) {
            save.println(tab.get(i).getType() + " " + tab.get(i).getX() + " " + tab.get(i).getY() + " " + tab.get(i).getStrength());
        }
        save.close();
    }

    public void loadGame() throws FileNotFoundException {
        Scanner load = new Scanner(new File("save.txt"));
        superpower=load.nextInt();
        String type = null;
        tab=new ArrayList<>();
        int x = 0,y=0,strength = 0;
        while(load.hasNext()) {
            type=load.next();
            x=load.nextInt();
            y=load.nextInt();
            strength=load.nextInt();

            if (type.equals("Human")) {
                tab.add(new Human(x, y, this, strength));
            }
            else if (type.equals("Antelope")) {
                tab.add(new Antelope(x, y, this, strength));
            }
            else if (type.equals("Fox")) {
                tab.add(new Fox(x, y, this, strength));
            }
            else if (type.equals("Turtle")) {
                tab.add(new Turtle(x, y, this, strength));
            }
            else if (type.equals("Sheep")) {
                tab.add(new Sheep(x, y, this, strength));
            }
            else if (type.equals("Wolf")) {
                tab.add(new Wolf(x, y, this, strength));
            }
            else if (type.equals("Dandelion")) {
                tab.add(new Dandelion(x, y, this));
            }
            else if (type.equals("Grass")) {
                tab.add(new Grass(x, y, this));
            }
            else if (type.equals("Guarana")) {
                tab.add(new Guarana(x, y, this));
            }
            else if (type.equals("SosnowskiHogweed")) {
                tab.add(new SosnowskiHogweed(x, y, this));
            }
            else if (type.equals("WolfBerries")) {
                tab.add(new WolfBerries(x, y, this));
            }
        }


    }
}
