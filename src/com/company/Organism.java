package com.company;

import java.awt.*;
import java.util.Random;

public abstract class Organism {
    protected static final int EMPTY = 100;
    protected static final int COLLISION = 99;
    protected static final int OUTOFBAND = 98;
    protected static final Random random = new Random();
    protected int x, y, strength, initiative;
    protected Color color;

    public Color getColor() {
        return color;
    }

    protected boolean isDead;
    protected String type;
    protected World world;

    public abstract void akcja();
    public abstract void addOrganism(int xx, int yy);

    public Organism(int x, int y, World world) {
        this.x = x;
        this.y = y;
        this.world = world;
        this.isDead=false;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getInitiative() {
        return initiative;
    }


    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type +
                "(x=" + x +
                ", y=" + y +
                ")";
    }

    public int canMove(int xx, int yy) {
        if (xx >= 1 && yy >= 1 && yy <= world.getH() && xx <= world.getW()) {
            if (world.findOrgBool(xx, yy) == false) {
                return EMPTY;
            }
            else if (world.findOrgBool(xx, yy)) {
                return COLLISION;
            }
        }
            return OUTOFBAND;
    }

    public void multiplication() {
        int temp_x, temp_y;
        if (y != 1 && world.findOrgBool(x, y - 1) == false) {
            addOrganism(x, y - 1);
            temp_x=x;
            temp_y=y-1;
            world.getEvents().add(this.toString() + " multipliacted to (" + temp_x +", " + temp_y+")");
        }
        //ucieczka w dół
        else if (y != world.getH() && world.findOrgBool(x, y + 1) == false) {
            addOrganism(x, y + 1);
            temp_x=x;
            temp_y=y+1;
            world.getEvents().add(this.toString() + " multiplicated to (" + temp_x +", " + temp_y+")");
        }
        //ucieczka w lewo
        else if (x != 1 && world.findOrgBool(x - 1, y) == false) {
            addOrganism(x - 1, y);
            temp_x=x-1;
            temp_y=y;
            world.getEvents().add(this.toString() + " multiplicated to (" + temp_x +", " + temp_y+")");
        }
        //ucieczka w prawo
        else if (x != world.getW() && world.findOrgBool(x + 1, y) == false) {
            addOrganism(x + 1, y);
            temp_x=x+1;
            temp_y=y;
            world.getEvents().add(this.toString() + " multiplicated to (" + temp_x +", " + temp_y+")");
        }
        //zamkniecie pliku
//        save.close();

    }

    public void kolizja(Organism attacker) {
        if (attacker.getStrength() >= strength && type != attacker.getType()) {
            this.setDead(true);
            //czy roslina
            if (this.getInitiative() != 0) {
//            save << attacker->toString() << " defeated " << this->toString() << endl;
                world.getEvents().add(attacker.toString() + " defeated " + this.toString());
            } else {
                world.getEvents().add(attacker.toString() + " ate " + this.toString());
            }
            attacker.setX(x);
            attacker.setY(y);
        } else {
            if (attacker.getStrength() < this.getStrength() && type != attacker.getType()) {
                world.getEvents().add(this.toString() + " defeated " + attacker.toString());
                attacker.setDead(true);
            } else {
                multiplication();
            }
//        save.close();
        }
    }

    public void removeItself() {
        int x = this.getX();
        int y = this.getY();
        for (int i = 0; i < world.getTab().size(); i++) {
            if (world.getTab().get(i).getX() == x && world.getTab().get(i).getY() == y && world.getTab().get(i).isDead()==true) {
                world.getTab().remove(i);
                break;
            }
        }
    }
}
