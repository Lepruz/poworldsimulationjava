package com.company.Plants;


import com.company.Plant;
import com.company.World;

import java.awt.*;

public class Grass extends Plant {
    public Grass(int x, int y, World world) {
        super(x, y, world);
        this.strength=0;
        this.type="Grass";
        this.color=Color.GREEN;
    }


    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Grass(xx,yy,world));
    }
}
