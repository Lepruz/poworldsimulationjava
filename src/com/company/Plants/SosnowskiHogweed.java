package com.company.Plants;


import com.company.Organism;
import com.company.Plant;
import com.company.World;

import java.awt.*;

public class SosnowskiHogweed extends Plant {
    public SosnowskiHogweed(int x, int y, World world) {
        super(x, y, world);
        this.strength=0;
        this.type="SosnowskiHogweed";
        this.color=Color.MAGENTA;
    }


    public void addOrganism(int xx, int yy) {
        world.getTab().add(new SosnowskiHogweed(xx,yy,world));
    }

    @Override
    public void akcja() {
        for (int i = -1; i < 2; i++) {
            for (int l = -1; l < 2; l++) {
                if (canMove(x + i, y + l) == COLLISION) {
                    if (world.findOrg(x + i, y + l).getInitiative() > 0) {
                        world.getEvents().add(world.findOrg(x + i, y + l).toString() + " is killed by " + this.toString());
                        world.findOrg(x + i, y + l).setDead(true);
                    }
                }
            }
        }
       super.akcja();
    }

    @Override
    public void kolizja(Organism attacker) {
        this.setDead(true);
        attacker.setDead(true);
        world.getEvents().add(attacker.toString() + " ate " + this.toString());
    }
}
