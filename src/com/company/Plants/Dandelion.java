package com.company.Plants;


import com.company.Plant;
import com.company.World;

import java.awt.*;

public class Dandelion extends Plant {
    public Dandelion(int x, int y, World world) {
        super(x, y, world);
        this.strength=0;
        this.initiative=0;
        this.type="Dandelion";
        this.color=Color.CYAN;
    }


    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Dandelion(xx,yy,world));
    }

    @Override
    public void akcja() {
        for(int i=0; i<3;i++) {
            super.akcja();
        }
    }
}
