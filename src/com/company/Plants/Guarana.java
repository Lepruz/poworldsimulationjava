package com.company.Plants;


import com.company.Organism;
import com.company.Plant;
import com.company.World;

import java.awt.*;

public class Guarana extends Plant {
    public Guarana(int x, int y, World world) {
        super(x, y, world);
        this.strength=0;
        this.type="Guarana";
        this.color=Color.RED;
    }


    public void addOrganism(int xx, int yy) {
        world.getTab().add(new Guarana(xx,yy,world));
    }

    @Override
    public void kolizja(Organism attacker) {
        this.setDead(true);
        attacker.setStrength(attacker.getStrength() + 3);
        attacker.setX(x);
        attacker.setY(y);
        world.getEvents().add(attacker.toString() + " ate " + this.toString());
    }
}
