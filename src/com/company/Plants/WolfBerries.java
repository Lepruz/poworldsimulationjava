package com.company.Plants;


import com.company.Organism;
import com.company.Plant;
import com.company.World;

import java.awt.*;

public class WolfBerries extends Plant {
    public WolfBerries(int x, int y, World world) {
        super(x, y, world);
        this.strength=0;
        this.type="WolfBerries";
        this.color=new Color(100,56,97);
    }


    public void addOrganism(int xx, int yy) {
        world.getTab().add(new WolfBerries(xx,yy,world));
    }

    @Override
    public void kolizja(Organism attacker) {
        this.setDead(true);
        attacker.setDead(true);
        world.getEvents().add(attacker.toString() + " ate " + this.toString());
    }
}
