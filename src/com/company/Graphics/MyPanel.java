package com.company.Graphics;

import com.company.Animals.*;
import com.company.Organism;
import com.company.Plants.*;
import com.company.World;

import javax.lang.model.type.NullType;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class MyPanel extends JPanel implements MouseListener {
    private int height, width,size;
    private World world;
    public MyPanel(int height, int width, World world) {
        this.height= height;
        this.width=width;
        this.world=world;
        this.size=50;
        addMouseListener(this);
        setLayout(new FlowLayout());
        setBackground(new Color(0,0,0));
        setPreferredSize(new Dimension(width,height));

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        for (int i=0; i<=height;i++) {
            for (int j=0; j<=width; j++) {
                g2d.setColor(Color.WHITE);
                g2d.drawRect(j*size,i*size,size-1,size-1);
                if(world.findOrgBool(j+1,i+1)) {
                        g2d.setColor(world.findOrg(j+1,i+1).getColor());
                }
                else {
                    g2d.setColor(Color.WHITE);
                }
                g2d.fillRect(j*size,i*size,size-2,size-2);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int x= e.getX();
        int y =e.getY();
        if (!world.findOrgBool(x / size + 1, y / size + 1)) {
            AnimalChoice choice = new AnimalChoice(world, x, y, this);
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
