package com.company.Graphics;

import com.company.Animals.*;
import com.company.Plants.*;
import com.company.World;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AnimalChoice extends JFrame implements ActionListener {
    private World world;
    private int x,y;
    private MyPanel myPanel;
    private JButton fox;
    private JButton antelope;
    private JButton sheep;
    private JButton turtle;
    private JButton wolf;
    private JButton dandelion;
    private JButton grass;
    private JButton guarana;
    private JButton sosnowskihogweed;
    private JButton wolfberries;

    public AnimalChoice(World world, int x, int y, MyPanel myPanel) throws HeadlessException {
        this.world = world;
        this.x = x;
        this.y = y;
        this.myPanel = myPanel;
        fox = new JButton("Fox");
        antelope = new JButton("Antelope");
        sheep = new JButton("Sheep");
        turtle = new JButton("Turtle");
        wolf = new JButton("Wolf");
        dandelion = new JButton("Dandelion");
        grass = new JButton("Grass");
        guarana = new JButton("Guarana");
        sosnowskihogweed = new JButton("Sosnowski");
        wolfberries = new JButton("Wolfberries");

        fox.addActionListener(this);
        antelope.addActionListener(this);
        sheep.addActionListener(this);
        turtle.addActionListener(this);
        wolf.addActionListener(this);
        dandelion.addActionListener(this);
        grass.addActionListener(this);
        guarana.addActionListener(this);
        sosnowskihogweed.addActionListener(this);
        wolfberries.addActionListener(this);

        setLayout(new FlowLayout());
        setPreferredSize(new Dimension(300, 300));
        setVisible(true);
        pack();
        add(fox);
        add(antelope);
        add(sheep);
        add(turtle);
        add(wolf);
        add(dandelion);
        add(grass);
        add(guarana);
        add(sosnowskihogweed);
        add(wolfberries);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (!world.findOrgBool(x / 50 + 1, y / 50 + 1)) {
            if (source == fox) {
                world.getTab().add(new Fox(x / 50 + 1, y / 50 + 1, world));
            } else if (source == antelope) {
                world.getTab().add(new Antelope(x / 50 + 1, y / 50 + 1, world));
            } else if (source == sheep) {
                world.getTab().add(new Sheep(x / 50 + 1, y / 50 + 1, world));
            } else if (source == turtle) {
                world.getTab().add(new Turtle(x / 50 + 1, y / 50 + 1, world));
            } else if (source == wolf) {
                world.getTab().add(new Wolf(x / 50 + 1, y / 50 + 1, world));
            } else if (source == dandelion) {
                world.getTab().add(new Dandelion(x / 50 + 1, y / 50 + 1, world));
            } else if (source == grass) {
                world.getTab().add(new Grass(x / 50 + 1, y / 50 + 1, world));
            } else if (source == guarana) {
                world.getTab().add(new Guarana(x / 50 + 1, y / 50 + 1, world));
            } else if (source == sosnowskihogweed) {
                world.getTab().add(new SosnowskiHogweed(x / 50 + 1, y / 50 + 1, world));
            } else if (source == wolfberries) {
                world.getTab().add(new WolfBerries(x / 50 + 1, y / 50 + 1, world));
            }
            myPanel.repaint();
        }
            dispose();
    }
}
