package com.company.Graphics;

import com.company.World;

import javax.swing.*;
import java.awt.*;
import java.util.List;


public class Comments extends JPanel {
    private World world;
    private static final int WIDTH = 800;
    private int height;

    public Comments(World world) {
        this.world = world;
        int height = 40*world.getEvents().size();
        setPreferredSize(new Dimension(WIDTH, 500));
        setLayout(new FlowLayout());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d=(Graphics2D) g;
        g2d.setFont(new Font("Times New Roman", Font.PLAIN,10));
        for (int i=0; i<world.getEvents().size();i++) {
            g2d.drawString(world.getEvents().get(i),100+(i/10)*WIDTH/2,(i%10+1)*15);
        }
    }
}
