package com.company.Graphics;

import com.company.Animals.Human;
import com.company.Organism;
import com.company.World;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class MyDraw extends JFrame{
    World world;
    MyPanel myPanel;

    public MyDraw(int width, int height, World world) {
        super("Jakub Citko 171946");
        this.world = world;
        myPanel = new MyPanel(height, width, world);
        Comments comments = new Comments(world);
        ButtonPanel buttonPanel = new ButtonPanel(world, myPanel, comments);
        setPreferredSize(new Dimension(width, height + 400));
        setLocation(50, 50);
        setLayout(new FlowLayout());

        add(myPanel);
        add(buttonPanel);
        add(comments);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }


//    @Override
//    public void keyReleased(KeyEvent e) {
//        int key = e.getKeyCode();
//        if (world.findHuman() != null) {
//            if (key == KeyEvent.VK_RIGHT) {
//                world.findHuman().setDirection(Human.getRIGHT());
//                world.turn();
//                myPanel.repaint();
//            } else if (key == KeyEvent.VK_LEFT) {
//                world.findHuman().setDirection(Human.getLEFT());
//                world.turn();
//                myPanel.repaint();
//            } else if (key == KeyEvent.VK_DOWN) {
//                world.findHuman().setDirection(Human.getDOWN());
//                world.turn();
//                myPanel.repaint();
//            } else if (key == KeyEvent.VK_UP) {
//                world.findHuman().setDirection(Human.getUP());
//                world.turn();
//                myPanel.repaint();
//            } else if (key == KeyEvent.VK_P) {
//                if (world.getSuperpower() == -1 && world.findHuman() != null) {
//                    world.setSuperpower(0);
//                }
//            }
//        }
//    }
}