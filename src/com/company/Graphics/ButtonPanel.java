package com.company.Graphics;

import com.company.Animals.Human;
import com.company.World;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileNotFoundException;

public class ButtonPanel extends JPanel implements KeyListener, ActionListener {
    public static final int HEIGHT = 150;
    public static final int WIDTH = 100;
    private JButton nextTurnButton;
    private JButton saveButton;
    private JButton loadButton;
    private JButton superpowerButton;
    private World world;
    private MyPanel myPanel;
    private Comments comments;

    public ButtonPanel(World world, MyPanel myPanel, Comments comments) {
        this.world = world;
        this.myPanel = myPanel;
        this.comments = comments;
        nextTurnButton = new JButton("Next turn");
        saveButton = new JButton("Save game");
        loadButton = new JButton("Load game");
        superpowerButton = new JButton("Superpower");
        nextTurnButton.addActionListener(this);
        saveButton.addActionListener(this);
        loadButton.addActionListener(this);
        superpowerButton.addActionListener(this);
        setFocusable(true);
        setLayout(new FlowLayout());
        setPreferredSize(new Dimension(WIDTH, HEIGHT));


        add(nextTurnButton);
        add(saveButton);
        add(loadButton);
        add(superpowerButton);
        addKeyListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == nextTurnButton) {
            world.turn();
            myPanel.repaint();
            comments.repaint();
        } else if (source == saveButton) {
            try {
                world.saveGame();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        } else if (source == loadButton) {
            try {
                world.loadGame();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            myPanel.repaint();
        }
        else if(source == superpowerButton) {
            if (world.getSuperpower() == -1 && world.findHuman() != null) {
                world.setSuperpower(0);
            }
        }
        this.setFocusable(true);
        this.requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if (world.findHuman() != null) {
            if (key == KeyEvent.VK_RIGHT) {
                world.findHuman().setDirection(Human.getRIGHT());
                world.turn();
                myPanel.repaint();
            } else if (key == KeyEvent.VK_LEFT) {
                world.findHuman().setDirection(Human.getLEFT());
                world.turn();
                myPanel.repaint();
            } else if (key == KeyEvent.VK_DOWN) {
                world.findHuman().setDirection(Human.getDOWN());
                world.turn();
                myPanel.repaint();
            } else if (key == KeyEvent.VK_UP) {
                world.findHuman().setDirection(Human.getUP());
                world.turn();
                myPanel.repaint();
            } else if (key == KeyEvent.VK_P) {
                if (world.getSuperpower() == -1 && world.findHuman() != null) {
                    world.setSuperpower(0);
                }
            }
        }
        if (key == KeyEvent.VK_N) {
            world.turn();
            myPanel.repaint();
        }
        if (key == KeyEvent.VK_S) {
            try {
                world.saveGame();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        }
        if (key == KeyEvent.VK_L) {
            try {
                world.loadGame();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            myPanel.repaint();
        }
        comments.repaint();
    }
}




