package com.company;

public abstract class Plant extends Organism {
    public Plant(int x, int y, World world) {
        super(x, y, world);
        this.initiative=0;
    }

    public void akcja() {
        int chance = random.nextInt(100);
        if (chance >= 0 && chance <= 3)
        {

            int temp_x = x + random.nextInt(3) - 1;
            int temp_y = y + random.nextInt(3) - 1;
            if (temp_x == x && temp_y == y)
                return;
            if (canMove(temp_x, temp_y) == EMPTY) {
                world.getEvents().add(this.toString() + " spread to (" + temp_x +", " + temp_y +")");
                addOrganism(temp_x, temp_y);

            }

        }
    }
}
